FROM alpine:latest

RUN apk add -U curl openssl wget

RUN echo "KubeCTL" \
    && apk add -U bash curl openssl \
    && curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl

